import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
  kotlin("jvm") version "1.3.41"
  groovy
  id("com.github.johnrengelman.shadow") version "5.1.0"
  application
  id("com.palantir.docker") version "0.20.1"
  id("com.energizedwork.webdriver-binaries") version "1.2"
}

val junitVersion by extra("5.5.1")
val spockVersion by extra("1.3-groovy-2.5")
val gebVersion by extra("3.0.1")
val seleniumVersion by extra("3.141.59")
val chromeDriverVersion by extra("77.0.3865.40")
val geckoDriverVersion by extra("0.25.0")
val ktorVersion by extra("1.2.2")
val koinVersion by extra("2.0.1")
val logbackVersion by extra("1.2.3")

val gebTestDrivers by extra(listOf("firefox", "chrome", "chromeHeadless"))

group = "com.accenture.cfe.dev"

version = "0.2"

repositories {
  mavenCentral()
  jcenter()
}
dependencies {
  implementation(kotlin("stdlib-jdk8"))
  implementation("io.ktor:ktor-server-netty:$ktorVersion")
  implementation("org.koin:koin-ktor:$koinVersion")
  implementation("ch.qos.logback:logback-classic:$logbackVersion")

  testImplementation("org.gebish:geb-spock:$gebVersion")
  //Drivers
  testImplementation("org.seleniumhq.selenium:selenium-chrome-driver:$seleniumVersion")
  testImplementation("org.seleniumhq.selenium:selenium-firefox-driver:$seleniumVersion")

  testImplementation("org.spockframework:spock-core:$spockVersion")
  testImplementation("org.junit.jupiter:junit-jupiter-api:$junitVersion")
  testRuntimeOnly("org.junit.vintage:junit-vintage-engine:$junitVersion")
}

webdriverBinaries {
  chromedriver = chromeDriverVersion
  geckodriver = geckoDriverVersion
}

sourceSets {
  create("integrationTest") {
    withConvention(GroovySourceSet::class) {
      groovy {
        setSrcDirs(listOf("src/testIntegration/groovy"))
      }
      resources.srcDir("src/testIntegration/resources")
      compileClasspath += sourceSets["test"].compileClasspath + configurations["testRuntimeClasspath"]
      runtimeClasspath += output + compileClasspath
    }
  }
}

gebTestDrivers.forEach {
  tasks.register<Test>("${it}IntegrationTest") {
    group = JavaBasePlugin.VERIFICATION_GROUP
    testClassesDirs = sourceSets["integrationTest"].output.classesDirs
    classpath = sourceSets["integrationTest"].runtimeClasspath
    outputs.upToDateWhen { false }
    systemProperty("geb.build.reportsDir", reporting.file("geb/$name"))
    systemProperty("geb.env", it)
  }
}

tasks {
  test {
    useJUnitPlatform()
  }

  shadowJar {
    archiveBaseName.set("rpn-calc")
    archiveClassifier.set(null as String?)
    archiveVersion.set(null as String?)
  }
}

tasks.withType<KotlinCompile> {
  kotlinOptions.jvmTarget = "1.8"
}

application {
  mainClassName = "io.ktor.server.netty.EngineMain"
}

docker {
  name = "rpn-calc:${version}"
  files(tasks["shadowJar"].outputs)
}
