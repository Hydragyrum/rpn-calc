import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.firefox.FirefoxDriver

waiting {
  timeout = 2
}

environments {
  chrome {
    driver = { new ChromeDriver() }
  }

  chromeHeadless {
    driver = {
      ChromeOptions opts = new ChromeOptions()
      opts.addArguments('headless')
      new ChromeDriver(opts)
    }
  }

  firefox {
    atCheckWaiting = 1
    driver = { new FirefoxDriver() }
  }
}

baseUrl = "http://localhost:8080"
