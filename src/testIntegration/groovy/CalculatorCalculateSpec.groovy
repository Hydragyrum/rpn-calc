import geb.spock.GebReportingSpec

import java.util.logging.Handler

class CalculatorCalculateSpec extends GebReportingSpec {

  def "Type a number on the calculator"() {
    given:
      to CalculatorPage

    when:
      one.click()

    then:
      display.text() == "1"
  }

  def "Calculate something"() {
    given:
      to CalculatorPage
    when:
      two.click()
    and:
      four.click()
    and:
      enter.click()
    and:
      one.click()
    and:
      eight.click()
    and:
      enter.click()
    and:
      plusSign.click()
    and:
      equalsSign.click()
    then:
    waitFor(1) {
      display.text() == '42'
    }
  }

  def "calculator error"() {
    given:
      to CalculatorPage
    when:
      four.click()
    and:
      two.click()
    and:
      timesSign.click()
    and:
      equalsSign.click()
    then:
      waitFor(1){
        display.text() == 'Err'
      }
  }
}
