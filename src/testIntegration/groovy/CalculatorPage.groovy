

import geb.Page

class CalculatorPage extends Page {
  static content = {
    equalsSign { $("#btn-equals") }
    plusSign { $("#btn-plus") }
    minusSign { $("#btn-minus") }
    timesSign { $("#btn-times") }
    divideSign { $("#btn-divide") }
    clear { $("#btn-clear") }
    del { $("#btn-del") }
    zero { $("#btn-zero") }
    one { $("#btn-one") }
    two { $("#btn-two") }
    three { $("#btn-three") }
    four { $("#btn-four") }
    five { $("#btn-five") }
    six { $("#btn-six") }
    seven { $("#btn-seven") }
    eight { $("#btn-eight") }
    nine { $("#btn-nine") }
    dot { $("#btn-dot") }
    enter { $("#btn-enter") }

    display { $("#display")}
//    calculator { module(CalculatorModule) }
  }

  static at = {
    title == "Calculator!"
  }
}
