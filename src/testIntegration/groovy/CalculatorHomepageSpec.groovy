import geb.spock.GebReportingSpec

class CalculatorHomepageSpec extends GebReportingSpec {

  def "Go to Calculator Page"() {
    given:
    to CalculatorPage

    expect:
    at CalculatorPage
  }
}
